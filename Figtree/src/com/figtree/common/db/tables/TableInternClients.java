/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.db.tables;

import com.figtree.common.db.Database;

/**
 * TableInternClients
 * @author FantaBlueMystery
 */
public class TableInternClients {

	/**
	 * database object
	 */
	private Database _db = null;

	/**
	 * TableInternClients
	 * @param db
	 */
	public TableInternClients(Database db) {
		this._db = db;

		this._db.createTable("CREATE TABLE IF NOT EXISTS internclients ("
			+ "id INTEGER PRIMARY KEY AUTO_INCREMENT, "
			+ "clientid VARCHAR(64), "
			+ "username VARCHAR(30), "
			+ "password VARCHAR(128), "
			+ "allowread INTEGER, "
			+ "allowwrite INTEGER, "
			+ "time_lastconnected DATETIME, "
			+ "time_lastmessage DATETIME);"
			);
	}


}