/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.config;

import com.figtree.common.config.json.JsonReader;
import java.util.ArrayList;

/**
 * ConfigProvider
 * @author FantaBlueMystery
 */
public class ConfigProvider {

	/**
	 * SingletonHolder
	 */
	static private final class SingletonHolder {
		static private final ConfigProvider INSTANCE = new ConfigProvider();
	}

	/**
	 * i
	 * return instnace
	 * @return
	 */
	static public ConfigProvider i() {
		return ConfigProvider.SingletonHolder.INSTANCE;
	}

	/**
	 * reader list
	 */
	protected ArrayList<IConfigReader> _readerList = new ArrayList<>();

	/**
	 * ConfigProvider
	 */
	private ConfigProvider() {
		this._readerList.add(new JsonReader());
	}

	/**
	 * readFile
	 * @param configfile
	 * @return
	 * @throws Exception
	 */
	public IConfig readFile(String configfile) throws Exception {
		return this._readerList.get(0).readFile(configfile);
	}
}