/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.config;

/**
 * IConfigReader
 * @author FantaBlueMystery
 */
public interface IConfigReader {

	/**
	 * readFile
	 * @param file
	 * @return
	 * @throws java.lang.Exception
	 */
	public IConfig readFile(String file) throws Exception;
}