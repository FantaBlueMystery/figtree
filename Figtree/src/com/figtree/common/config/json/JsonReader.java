/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.config.json;

import com.figtree.common.config.IConfig;
import com.figtree.common.config.IConfigReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * JsonReader
 * @author FantaBlueMystery
 */
public class JsonReader implements IConfigReader {

	/**
	 * readFile
	 * @param file
	 * @return
	 */
	@Override
	public IConfig readFile(String file) throws Exception {
		if( !file.endsWith(".json") ) {
			throw new Exception("Config not a json file!");
		}

		File fFile = new File(file);

		if( !(fFile).exists() ) {
			throw new FileNotFoundException();
		}


		return new JsonConfig(this._readFile(fFile));
	}

	/**
	 * _readFile
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private String _readFile(File file) throws FileNotFoundException, IOException {
		StringBuilder result = new StringBuilder();

		try( BufferedReader reader = new BufferedReader(new FileReader(file)) ) {
			char[] buf = new char[1024];

			int r = 0;

			while ((r = reader.read(buf)) != -1) {
				result.append(buf, 0, r);
			}

			reader.close();
		}

		return result.toString();
	}
}