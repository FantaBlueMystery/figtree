/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.config.json;

import com.figtree.common.config.IConfig;
import org.json.JSONObject;

/**
 * JsonConfig
 * @author FantaBlueMystery
 */
public class JsonConfig implements IConfig {

	/**
	 * data
	 */
	protected JSONObject _data;

	/**
	 * JsonConfig
	 */
	public JsonConfig() {
		this._data = new JSONObject();
	}

	/**
	 * JsonConfig
	 * @param content
	 */
	public JsonConfig(String content) {
		this._data = new JSONObject(content);
	}

	/**
	 * put
	 * @param key
	 * @param value
	 */
	@Override
	public void put(String key, Object value) {
		this._data.put(key, value);
	}

	/**
	 * get
	 * @param key
	 * @return
	 */
	@Override
	public String get(String key) {
		return (String) this._data.get(key);
	}

	/**
	 * has
	 * @param key
	 * @return
	 */
	@Override
	public boolean has(String key) {
		return this._data.has(key);
	}

	/**
	 * cast
	 * @param config
	 * @return
	 */
	public JsonConfig cast(JsonConfig config) {
		config._data = this._data;

		return config;
	}
}