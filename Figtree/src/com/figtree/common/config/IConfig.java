/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.config;

/**
 * IConfig
 * @author FantaBlueMystery
 */
public interface IConfig {

	/**
	 * put
	 * @param key
	 * @param value
	 */
	public void put(String key, Object value);

	/**
	 * get
	 * @param key
	 * @return
	 */
	public String get(String key);

	/**
	 * has
	 * @param key
	 * @return
	 */
	public boolean has(String key);
}
