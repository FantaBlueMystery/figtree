/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common.mqtt;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * MqttClient
 * @author FantaBlueMystery
 */
public class FigTreeMqttClient implements MqttCallback {

	/**
	 * mqtt client
	 */
	private IMqttClient _client;

	/**
	 * FigTreeMqttClient
	 */
	public FigTreeMqttClient() {
		this.init();
	}

	/**
	 * init
	 */
	protected void init() {
		String publisherId = "Login-" + UUID.randomUUID().toString();

		try {
			this._client = new MqttClient("tcp://127.0.0.1:1883", publisherId, new MemoryPersistence());

			MqttConnectOptions options = new MqttConnectOptions();

			options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			//options.setConnectionTimeout(10);
			options.setUserName("test");
			options.setPassword("test".toCharArray());

			this._client.setCallback(this);
			this._client.connect(options);
		}
		catch( MqttException ex ) {
			Logger.getLogger(FigTreeMqttClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * sendMessage
	 * @param content
	 * @throws MqttException
	 */
	public void sendMessage(String content) throws MqttException {
		if( this._client.isConnected() ) {

			MqttMessage message = new MqttMessage(content.getBytes());
			this._client.publish("test-topic", message);
		}
	}

	/**
	 * connectionLost
	 * @param cause
	 */
	@Override
	public void connectionLost(Throwable cause) {

	}

	/**
	 * messageArrived
	 * @param topic
	 * @param message
	 * @throws Exception
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

	}

	/**
	 * deliveryComplete
	 * @param token
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}
}