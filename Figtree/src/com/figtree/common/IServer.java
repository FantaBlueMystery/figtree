/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.common;

/**
 * IServer
 * @author FantaBlueMystery
 */
public interface IServer {

	/**
	 * start
	 */
	public void start();

	/**
	 * stop
	 */
	public void stop();

	/**
	 * reload
	 */
	public void reload();
}