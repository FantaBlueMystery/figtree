/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.login.config;

import com.figtree.common.config.json.JsonConfig;
import static com.figtree.login.config.LoginConfig.PORT_PROPERTY_NAME;

/**
 * LoginConfig
 * @author FantaBlueMystery
 */
public class LoginConfig extends JsonConfig {

	static public final String PORT_PROPERTY_NAME = "port";

	/**
	 * getPort
	 * @return
	 */
	public int getPort() {
		int port = 9010;

		if( this.has(PORT_PROPERTY_NAME) ) {
			port = Integer.valueOf(this.get(PORT_PROPERTY_NAME));
		}

		return port;
	}
}