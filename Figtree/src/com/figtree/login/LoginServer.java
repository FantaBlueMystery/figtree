/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.login;

import com.figtree.common.IServer;
import com.figtree.common.config.ConfigProvider;
import com.figtree.common.config.json.JsonConfig;
import com.figtree.common.mqtt.FigTreeMqttClient;
import com.figtree.login.config.LoginConfig;
import com.figtree.server.IServerDaemon;
import com.jfiesta.lib.Factory;
import com.jfiesta.lib.net.server.ServerSocketLogin;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * LoginServer
 * @author FantaBlueMystery
 */
public class LoginServer implements IServer, IServerDaemon {

	/**
	 * config
	 */
	protected LoginConfig _config;

	/**
	 * server socket login
	 */
	protected ServerSocketLogin _serverLogin;

	/**
	 * LoginServer
	 * @param configFile
	 */
	public LoginServer(String configFile) {
		try {
			JsonConfig config = (JsonConfig) ConfigProvider.i().readFile(configFile);

			this._config = (LoginConfig) config.cast(new LoginConfig());

			Factory.i().getSettings();
			Factory.i().initShn();

			FigTreeMqttClient internClient = new FigTreeMqttClient();
			//internClient.sendMessage("Hello");
			//this.start();
		}
		catch( Exception ex ) {
			ex.printStackTrace();
		}
	}

	/**
	 * start
	 */
	@Override
	public void start() {
		this._serverLogin = new ServerSocketLogin(this._config.getPort());

		try {
			this._serverLogin.start();

			System.out.println("FigTree Login Server started, press ctrl-c to shutdown..");
		}
		catch( IOException ex ) {
			this._serverLogin = null;
		}
	}

	/**
	 * stop
	 */
	@Override
	public void stop() {
		if( this._serverLogin != null ) {
			try {
				this._serverLogin.stop();
			}
			catch( IOException ex ) {
				Logger.getLogger(LoginServer.class.getName()).log(Level.SEVERE, null, ex);
			}

			this._serverLogin = null;
		}
	}

	/**
	 * reload
	 */
	@Override
	public void reload() {
		this.stop();
		this.start();
	}
}