/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.figtree;

/**
 * ShutdownThread
 * @author FantaBlueMystery
 */
public class ShutdownThread extends Thread {

	/**
     * daemon
     */
    protected FigTreeDaemon _daemon = null;

    /**
     * constructor
     * @param daemon
     */
    public ShutdownThread(FigTreeDaemon daemon) {
        this._daemon = daemon;
    }

    /**
     * run
     */
    @Override
    public void run() {
        this._daemon.stop();
    }
}