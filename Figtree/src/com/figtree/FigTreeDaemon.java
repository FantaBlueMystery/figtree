/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.figtree;

import com.figtree.command.ThreadCommandReader;
import com.figtree.server.IServerDaemon;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FigTreeDaemon
 * @author FantaBlueMystery
 */
public class FigTreeDaemon implements Runnable {

	/**
     * instance of daemon
     */
    static protected FigTreeDaemon _daemon;

	/**
     * intern logger
     */
    static protected Logger _log = Logger.getLogger(FigTreeDaemon.class.getName());

	/**
     * _setDaemon
     * @param daemon
     */
    static protected void _setDaemon(FigTreeDaemon daemon) {
        if( FigTreeDaemon._daemon != null) {
            throw new UnsupportedOperationException("Cannot redefine singleton daemon");
        }

        FigTreeDaemon._daemon = daemon;

		// TODO Log info
    }

    /**
     * getDaemon
     * @return
     */
    static public FigTreeDaemon getDaemon() {
        return FigTreeDaemon._daemon;
    }

	/**
     * ticks
     */
    protected int _ticks = 0;

    /**
     * is running
     */
    protected boolean _isRunning = true;

    /**
     * is stopped daemon
     */
    protected Boolean _isStopped = false;

	/**
     * use thread command reader
     */
    protected Boolean _useThreadCommandReader = true;

	/**
	 * server
	 */
	protected IServerDaemon _server;

	/**
	 * FigTreeDaemon
	 * @param server
	 */
	public FigTreeDaemon(IServerDaemon server) {
		this._server = server;

		FigTreeDaemon._setDaemon(this);
		Runtime.getRuntime().addShutdownHook(new ShutdownThread(this));
	}

	/**
     * isRunning
     * @param daemon
     * @return
     */
    static boolean isRunningDaemon(FigTreeDaemon daemon) {
        return daemon.isRunning();
    }

    /**
     * isStopped
     * @return
     */
    public boolean isStopped() {
        return this._isStopped;
    }

	/**
	 * isRunning
	 * @return
	 */
	public boolean isRunning() {
		return this._isRunning;
	}

	/**
     * safeShutdown
     */
    public void safeShutdown() {
        this._isRunning = false;
    }

    /**
     * shutdown
     */
    public static void shutdown() {
        FigTreeDaemon.getDaemon().safeShutdown();
    }

	/**
     * _init
     * @return
     */
    protected Boolean _init() {
		if( this._useThreadCommandReader ) {
			ThreadCommandReader threadcommandreader = new ThreadCommandReader(this);
			threadcommandreader.setDaemon(true);
			threadcommandreader.start();
		}

		return true;
	}

	/**
     * getLogger
     * @return
     */
    public Logger getLogger() {
		return FigTreeDaemon._log;
	}

	/**
     * stop
     */
    public void stop() {
		FigTreeDaemon._log.info("Stopping FigTree");
	}

	/**
     * _action
     */
    protected void _action() {
		try {
            ++this._ticks;
		}
        catch( Exception exception ) {
			FigTreeDaemon._log.log(Level.WARNING,
                "Unexpected exception while parsing console command",
                exception
                );
		}
	}

	/**
     * reload
     */
    static public void reload() {
		FigTreeDaemon tdaemon = FigTreeDaemon.getDaemon();

	}

	/**
     * issueCommand
     * @param s
     * @param daemon
     */
    public void issueCommand(String s, FigTreeDaemon daemon) {
		System.out.println("Debugging");
	}

	/**
	 * run
	 */
	@Override
	public void run() {
		try {
			if( this._init() ) {
				long i = System.currentTimeMillis();

				for( long j=0L; this._isRunning; Thread.sleep(1L) ) {
                    long k = System.currentTimeMillis();
                    long l = k - i;

					if( l>2000L ) {
						FigTreeDaemon._log.warning(
							"Can\'t keep up! Did the system time change, or is the server overloaded?");

						l = 2000L;
					}

					if( l<0L ) {
						FigTreeDaemon._log.warning("Time ran backwards! Did the system time change?");

						l = 0L;
					}

					j += l;
                    i = k;

                    while( j > 50L ) {
                        j -= 50L;

                        this._action();
                    }
				}
			}
			else {
                while( this._isRunning ) {
					//this.executCommandList();

					try {
                        Thread.sleep(10L);
                    }
                    catch( InterruptedException interruptedexception ) {
                        interruptedexception.printStackTrace();
                    }
				}
			}
		}
		catch( Throwable throwable ) {
            throwable.printStackTrace();

			FigTreeDaemon._log.log(Level.SEVERE, "Unexpected exception", throwable);

			while( this._isRunning ) {
                //this.executCommandList();

                try {
                    Thread.sleep(10L);
                }
                catch( InterruptedException interruptedexception ) {
                    interruptedexception.printStackTrace();
                }
            }
		}
		finally {
            try {
                this.stop();
                this._isStopped = true;
            }
            catch( Throwable throwable1 ) {
                throwable1.printStackTrace();
            }
            finally {
                System.exit(0);
            }
        }
	}
}