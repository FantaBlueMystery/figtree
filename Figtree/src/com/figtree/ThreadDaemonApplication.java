/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.figtree;

/**
 * ThreadDaemonApplication
 * @author FantaBlueMystery
 */
public class ThreadDaemonApplication extends Thread {

	final FigTreeDaemon a;

	/**
     * constructor
     * @param name
     * @param daemon
     */
    public ThreadDaemonApplication(String name, FigTreeDaemon daemon) {
        super(name);
        this.a = daemon;
    }

	/**
     * run
     */
    @Override
    public void run() {
        this.a.run();
    }
}
