/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.figtree.command;

import com.figtree.FigTreeDaemon;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * ThreadCommandReader
 * @author FantaBlueMystery
 */
public class ThreadCommandReader extends Thread {

	/**
     * daemon
     */
    final FigTreeDaemon _daemon;

    /**
     * ThreadCommandReader
     * @param daemon
     */
    public ThreadCommandReader(FigTreeDaemon daemon) {
        this._daemon = daemon;
    }

	/**
     * run
     */
    @Override
    public void run() {
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;

        try {

            while(
				!this._daemon.isStopped() &&
				FigTreeDaemon.getDaemon().isRunning() &&
				((s = bufferedreader.readLine()) != null) )
            {
                this._daemon.issueCommand(s, this._daemon);
            }
        }
        catch( IOException ioexception ) {
            ioexception.printStackTrace();
        }
    }
}