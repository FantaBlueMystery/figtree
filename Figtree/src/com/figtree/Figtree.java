/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree;

import com.figtree.intern.InternServer;
import com.figtree.login.LoginServer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Figtree
 * @author FantaBlueMystery
 */
public class Figtree {

	/**
	 * main
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		// TODO, quick and dirty for tests
		org.apache.log4j.BasicConfigurator.configure();
		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);

		List<String> argsList				= new ArrayList<>();
		HashMap<String, String> optsList	= new HashMap<String, String>();
		List<String> doubleOptsList			= new ArrayList<>();

		try {
			for( int i=0; i<args.length; i++ ) {
				switch( args[i].charAt(0) ) {
					case '-':

						if( args[i].length() < 2 ) {
							throw new IllegalArgumentException("Not a valid argument: " + args[i]);
						}

						if( args[i].charAt(1) == '-' ) {
							if( args[i].length() < 3 ) {
								throw new IllegalArgumentException("Not a valid argument: " + args[i]);
							}

							doubleOptsList.add(args[i].substring(2, args[i].length()));
						}
						else {
							if( (args.length-1) == i ) {
								throw new IllegalArgumentException("Expected arg after: " + args[i]);
							}

							optsList.put(args[i], args[i+1]);

							i++;
						}

						break;

					default:
						argsList.add(args[i]);
						break;
				}
			}
		}
		catch( IllegalArgumentException ex ) {
			Logger.getLogger(Figtree.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}

		// ---------------------------------------------------------------------

		try {
			for( String command: optsList.keySet() ) {
				switch( command ) {
					case "-server-login":
						(new ThreadDaemonApplication("Daemon thread login",
							new FigTreeDaemon(
								new LoginServer(optsList.get(command))
								))
							).start();
						break;

					case "-server-world":
						break;

					case "-server-zone":
						break;

					case "-server-intern":
						(new ThreadDaemonApplication("Daemon thread intern",
							new FigTreeDaemon(
								new InternServer(optsList.get(command))
								))
							).start();
						break;
				}
			}
		}
        catch( Exception exception ) {
            exception.printStackTrace();
        }
	}
}