/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.pluginsystem;

/**
 * Pluggable
 * @author FantaBlueMystery
 */
public interface IPluggable {

	/**
	 * load
	 * @return
	 */
	public boolean load();

	/**
	 * unload
	 */
	public void unload();

	/**
	 * start
	 * @return
	 */
	public boolean start();

	/**
	 * stop
	 * @return
	 */
	public boolean stop();

	/**
	 * setPluginManager
	 * @param manager
	 */
	public void setPluginManager(IPluggableManager manager);
}