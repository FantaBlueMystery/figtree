/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.pluginsystem;

/**
 * PluginManager
 * @see https://www.java-blog-buch.de/d-plugin-entwicklung-in-java/2/
 * @author FantaBlueMystery
 */
public class PluginManager implements IPluggableManager {


}