/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.utils;

/**
 * Util
 * @author FantaBlueMystery
 */
public class Util {

	/**
	 * printSection
	 * @param s
	 */
	static public void printSection(String s) {
		if( !s.isEmpty() ) {
			s = "[ " + s + " ]";
		}

		while( s.length() < 79 ) {
			s = "=" + s + "=";
		}

		System.out.println("");
		System.out.println(s);
		System.out.println("");
	}

	/**
	 * printSsSection
	 * @param s
	 */
	static public void printSsSection(String s) {
		s = "( " + s + " )";

		while( s.length() < 79 ) {
			s = "-" + s + "-";
		}

		System.out.println("");
		System.out.println(s);
		System.out.println("");
	}

	/**
	 * printProgressBarHeader
	 * @param size
	 */
	static public void printProgressBarHeader(int size) {
		StringBuilder header = new StringBuilder("0%[");

		for( int i=0; i<size; i++ ) {
			header.append("-");
		}

		header.append("]100%");

		System.out.println(header);
		System.out.print("   ");
	}

	/**
	 * printCurrentProgress
	 */
	static public void printCurrentProgress() {
		System.out.print("+");
	}

	/**
	 * printEndProgress
	 */
	static public void printEndProgress() {
		System.out.print(" Done. \n");
	}
}