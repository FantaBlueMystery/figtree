/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.intern.listener;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.messages.InterceptConnectMessage;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBuf;

/**
 * MqttListener
 * @author FantaBlueMystery
 */
public class MqttListener extends AbstractInterceptHandler {

	/**
	 * ID
	 */
	private static final String ID = "figtree-intern";

	/**
	 * getID
	 * @return
	 */
	@Override
	public String getID() {
		return MqttListener.ID;
	}

	/**
	 * onPublish
	 * @param message
	 */
	@Override
	public void onPublish(InterceptPublishMessage message) {
		System.out.println("moquette mqtt broker message intercepted, topic: " + message.getTopicName()
				+ ", content: ");

		ByteBuf buffer = message.getPayload();
		byte[] bytes = new byte[buffer.readableBytes()];
		buffer.readBytes(bytes);
		String payload = new String(bytes);

		System.out.println(payload);
	}

	/**
	 *
	 * @param msg
	 */
	@Override
	public void onConnect(InterceptConnectMessage msg) {
		System.out.println("onConnect: " + msg.getClientID());
	}
}