/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.intern;

import com.figtree.common.IServer;
import com.figtree.common.config.ConfigProvider;
import com.figtree.common.config.json.JsonConfig;
import com.figtree.intern.config.InternConfig;
import com.figtree.intern.listener.MqttListener;
import com.figtree.intern.security.MqttAuthenticator;
import com.figtree.intern.security.MqttAuthorizator;
import com.figtree.server.IServerDaemon;
import io.moquette.interception.InterceptHandler;
import io.moquette.server.Server;
import io.moquette.server.config.IConfig;
import io.moquette.server.config.MemoryConfig;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * InternServer
 * @author FantaBlueMystery
 */
public class InternServer implements IServer, IServerDaemon {

	// https://www.hascode.com/2016/06/playing-around-with-mqtt-and-java-with-moquette-and-eclipse-paho/

	/**
	 * Server config
	 */
	protected InternConfig _config;

	/**
	 * mqtt broker server
	 */
	protected Server _mqttBroker;

	/**
	 * InternServer
	 * @param configFile
	 */
	public InternServer(String configFile) {
		/*org.apache.log4j.Logger logger4j = org.apache.log4j.Logger.getRootLogger();
		logger4j.setLevel(org.apache.log4j.Level.toLevel("ERROR"));

		try {
			JsonConfig config = (JsonConfig) ConfigProvider.i().readFile(configFile);

			this._config = (InternConfig) config.cast(new InternConfig());

			InternServer self = this;

			Runtime.getRuntime().addShutdownHook(new Thread() {

				@Override
				public void run() {
					self.stop();
				}
			});

			this.start();
		}
		catch( Exception ex ) {
			ex.printStackTrace();
		}*/
	}

	/**
	 * start
	 */
	@Override
	public void start() {
		try {
			this._mqttBroker = new Server();

			List<? extends InterceptHandler> internHandlers = Arrays.asList(new MqttListener());

			MqttAuthenticator authenticator = new MqttAuthenticator();
			MqttAuthorizator authorizator	= new MqttAuthorizator();

			IConfig config = new MemoryConfig(this._config.toProperties());

			this._mqttBroker.startServer(
				config,
				internHandlers,
				null,
				authenticator,
				authorizator
				);

			System.out.println("FigTree Intern mqtt broker started, press ctrl-c to shutdown..");
		}
		catch( IOException ex ) {

		}
	}

	/**
	 * stop
	 */
	@Override
	public void stop() {
		if( this._mqttBroker != null ) {
			System.out.println("stopping FigTree Intern mqtt broker..");

			this._mqttBroker.stopServer();

			System.out.println("FigTree Intern mqtt broker stopped");

			this._mqttBroker = null;
		}
	}

	/**
	 * reload
	 */
	@Override
	public void reload() {
		this.stop();
		this.start();
	}
}