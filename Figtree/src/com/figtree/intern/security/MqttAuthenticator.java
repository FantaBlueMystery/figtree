/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.intern.security;

import io.moquette.spi.security.IAuthenticator;

/**
 * MqttAuthenticator
 * @author FantaBlueMystery
 */
public class MqttAuthenticator implements IAuthenticator {

	/**
	 * checkValid
	 * @param clientId
	 * @param username
	 * @param password
	 * @return
	 */
	@Override
	public boolean checkValid(String clientId, String username, byte[] password) {
		System.out.println("checkValid: " + clientId + " username: " + username + " Passwort: " + new String(password));
		return true;
	}
}
