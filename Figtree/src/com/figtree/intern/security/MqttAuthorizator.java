/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.intern.security;

import io.moquette.spi.impl.subscriptions.Topic;
import io.moquette.spi.security.IAuthorizator;

/**
 * MqttAuthorizator
 * @author FantaBlueMystery
 */
public class MqttAuthorizator implements IAuthorizator {

	/**
	 * canWrite
	 * @param topic
	 * @param user
	 * @param client
	 * @return
	 */
	@Override
	public boolean canWrite(Topic topic, String user, String client) {
		return true;
	}

	/**
	 * canRead
	 * @param topic
	 * @param user
	 * @param client
	 * @return
	 */
	@Override
    public boolean canRead(Topic topic, String user, String client) {
		return true;
	}
}