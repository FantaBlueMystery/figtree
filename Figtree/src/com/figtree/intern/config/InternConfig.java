/**
 * FigTree - Server
 *
 * @author FantaBlueMystery
 * @copyright 2019 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.figtree.intern.config;

import com.figtree.common.config.json.JsonConfig;
import io.moquette.BrokerConstants;
import java.util.Properties;

/**
 * InternConfig
 * @author FantaBlueMystery
 */
public class InternConfig extends JsonConfig {

	/**
	 * toProperties
	 * @return
	 */
	public Properties toProperties() {
		Properties props = new Properties();

		if( this.has(BrokerConstants.PORT_PROPERTY_NAME) ) {
			props.put(BrokerConstants.PORT_PROPERTY_NAME, this.get(BrokerConstants.PORT_PROPERTY_NAME));
		}
		else {
			props.put(BrokerConstants.PORT_PROPERTY_NAME, BrokerConstants.PORT);
		}

		if( this.has(BrokerConstants.HOST_PROPERTY_NAME) ) {
			props.put(BrokerConstants.HOST_PROPERTY_NAME, this.get(BrokerConstants.HOST_PROPERTY_NAME));
		}
		else {
			props.put(BrokerConstants.HOST_PROPERTY_NAME, "127.0.0.1");
		}

		return props;
	}
}
